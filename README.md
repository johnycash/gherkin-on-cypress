# Gherkin on Cypress
czyli niekonwencjonalne metody uprawy kiszonych ogórków na cyprysach.
## Requirements
* docker, docker-compose
### Local execution and development
* nodejs 8+ installed
* *127.0.0.1 gameofthrones.local* in /etc/hosts
## Running locally
* Execute *docker-compose -f docker-compose.apponly.yml*

In test:

* *npm install*
* *./node_modules/cypress/bin/cypress install*
* *./node_modules/cypress/bin/cypress open*
## Running headless
* *docker-compose up -d*

## See also

[Nightwatch, Cucumber and Game Of Thrones](https://bitbucket.org/johnycash/nightwatch-cucumber-gameofthrones/src/step_5_testing_essos/)
