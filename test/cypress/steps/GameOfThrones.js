import { Given } from "cypress-cucumber-preprocessor/steps";
import { When } from "cypress-cucumber-preprocessor/steps";
import { Then } from "cypress-cucumber-preprocessor/steps";

const url = 'http://gameofthrones.local:3000'

Given('I am the Khaleesi of the Great Grass Sea', () => {
  cy.visit(url + '/essos');
});

Given('I have {int} dragon eggs', function (eggNumber) {
  var eggCounter = eggNumber;
  while(eggCounter--) {
    cy
      .get('.createEgg')
      .click();
  }
});

When('I put {int} eggs on funeral pyre', function (eggNumber) {
  var eggCounter = eggNumber;

    while(eggCounter--) {
      cy
        .get('.eggbox .egg:first-of-type')
        .trigger('touchstart', { which: 1})
        .wait(100);

      cy
        .get('.pyre')
        .trigger("mousemove")
        .wait(100)
        .trigger("touchend")
        .wait(100);
    }
});

When('I set the fire', function () {
  cy
    .get('.setfire')
    .click();
});

When('I wait some time', function () {
  cy
    .wait(5000);
});

Then('I have {int} young dragons', function (dragonsCount) {
  cy
    .get(`.pyre > .dragon:nth-of-type(${dragonsCount})`).should('be.visible');
  cy
    .get(`.pyre > .dragon:nth-of-type(${dragonsCount + 1})`).should('be.not.visible');
});

Given('I am the queen of Vesteros', () => {
  cy.visit(url + '/vesteros');
});

When(/^I hit the ['](.*)['] family$/, function (familyName) {
  cy
    .get('.family.' + familyName.toLowerCase())
    .click();
});

Then(/^The ['](.*)['] family breaks$/, function (familyName) {
  cy
    .get('.family.' + familyName.toLowerCase())
    .should('have.class', 'broken');
});
